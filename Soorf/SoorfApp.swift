//
//  SoorfApp.swift
//  Soorf
//
//  Created by Garat Berasategi on 18/5/23.
//

import SwiftUI
import TipKit

@main
struct SoorfApp: App {
    
    var body: some Scene {
        WindowGroup {
            MainView()
                .task {
                    try? Tips.configure([.datastoreLocation(.applicationDefault)])
                }
        }
        .modelContainer(for: [UserInfo.self,FavoriteBeach.self,AppConfigurations.self])
        
        
    }
    init() {
            //print(URL.applicationSupportDirectory.path(percentEncoded: false))
        }
}

