//
//  SubscriptionCheck.swift
//  Soorf
//
//  Created by Garat Berasategi on 14/3/24.
//

import SwiftUI
import MapKit
import SwiftData
import Foundation


struct SubscriptionCheck: View {
    @Environment(\.modelContext) var context
    @EnvironmentObject var mapControl: MapControl

    @Query var userData: [UserInfo]

    @Binding var userIsSubscribed: Bool?
    
    var body: some View {
        //Condition to show either paywall or surf conditions
        if (userIsSubscribed! || userData.isEmpty || userData[0].numberOfCalls <= 4 ){
            WeatherView()
                .environmentObject(mapControl)
                .onDisappear{userData[0].numberOfCalls += 1}
        }
        else {
            Subscription_View()
        }
    }
}

#Preview {
    SubscriptionCheck(userIsSubscribed: MainView().$isPro)
}
