//
//  Subscription View.swift
//  Soorf
//
//  Created by Garat Berasategi on 12/3/24.
//

import SwiftUI
import StoreKit

struct Subscription_View: View {
    
    var body: some View {
        
        SubscriptionStoreView(groupID: "21346008")
            .storeButton(.visible, for: .cancellation, .policies)
            .subscriptionStoreControlStyle(.prominentPicker)
            .onInAppPurchaseCompletion { _, purchaseResult in
                guard case .success(let verificationResult) = purchaseResult,
                      case .success(_) = verificationResult else {
                    return
                }
            }
            .subscriptionStorePolicyDestination(for: .privacyPolicy) {
                VStack{
                    Link(destination: URL(string: "https://www.freeprivacypolicy.com/live/43414261-e00e-484f-9216-fdee73b59dc9")!) {
                        Image(systemName: "link.circle.fill")
                            .font(.largeTitle)
                    }
                    Text("Click to Read")
                        .font(.headline)
                }
            }
            .subscriptionStorePolicyDestination(for: .termsOfService) {
                VStack{
                    Link(destination: URL(string: "https://www.apple.com/legal/internet-services/itunes/dev/stdeula/")!) {
                        Image(systemName: "link.circle.fill")
                            .font(.largeTitle)
                    }
                    Text("Click to Read")
                        .font(.headline)
                }
            }
    }
}

#Preview {
    Subscription_View()
}
