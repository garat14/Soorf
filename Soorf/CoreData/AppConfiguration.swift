//
//  AppConfiguration.swift
//  Soorf
//
//  Created by Garat Berasategi on 29/5/24.
//

import Foundation
import SwiftData
import MapKit

enum tempUnit: Codable {
    case celsius, farenheit
}

enum lengthUnit: Codable {
    case meter, feet
}


@Model class AppConfigurations{
    //Units
    var tempUnit: tempUnit
    var lengthUnit: lengthUnit
    
    //Sea Conditions
    var maxWave: Float
    var minWave: Float
    var maxSwell: Float
    var minSwell: Float
    var maxWind: Float
    var minWind: Float
    
    init() {
        self.tempUnit = .celsius
        self.lengthUnit = .meter
        
        self.maxWave = 15
        self.minWave = 0
        self.maxSwell = 15
        self.minSwell = 0
        self.maxWind = 15
        self.minWind = 0
    }
}
