//
//  FavoriteBeach.swift
//  Soorf
//
//  Created by Garat Berasategi on 25/3/24.
//

import Foundation
import SwiftData
import MapKit

@Model class FavoriteBeach {
    @Attribute(.unique) var beachName: String
    var latitude: Float
    var longitude: Float
    
    init(beachName: String, latitude: Float,longitude: Float) {
        self.beachName = beachName
        self.latitude = latitude
        self.longitude = longitude
    }
}
