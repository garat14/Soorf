//
//  UsageDataEntity.swift
//  Soorf
//
//  Created by Garat Berasategi on 15/3/24.
//
//

import Foundation
import SwiftData


@Model public class UserInfo {
    var isTipShowing: Bool
    var numberOfCalls: Int16
    public init() {
        self.isTipShowing = true
        self.numberOfCalls = 0
    }
    
}
