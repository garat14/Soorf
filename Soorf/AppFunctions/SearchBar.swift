//
//  SearchBar.swift
//  Soorf
//
//  Created by Garat Berasategi on 2/1/24.
//

import SwiftUI
import Foundation
import MapKit

struct SearchBar: View {
    
    @EnvironmentObject var mapControl: MapControl
    
    @State private var locationService = LocationService(completer: .init())
    @State private var detents: PresentationDetent = .height(100)
    
    @Binding var activeSheet: activeSheet?
    @Binding var search: String
    @FocusState private var nameIsFocused: Bool
    @Binding var searchResults: [SearchResult]
    
    var body: some View {
        
        VStack{
            HStack {
                Image(systemName: "magnifyingglass")
                TextField("Find your beach", text: $search)
                    .focused($nameIsFocused)
            }
            .autocorrectionDisabled()
            .padding(12)
            .background(Color(.systemBackground).opacity(0.8))
            .clipShape(Capsule())
            .onSubmit {
                Task {
                    searchResults = (try? await locationService.search(with: search)) ?? []
                }
                activeSheet = .none
            }
            Spacer()
            List {
                ForEach(locationService.completions) { completion in
                    Button(action: {
                        nameIsFocused = false
                        detents = .height(100)
                        didTapOnCompletion(completion)
                    }) {
                        VStack(alignment: .leading, spacing: 4) {
                            Text(completion.title)
                                .font(.headline)
                                .fontDesign(.rounded)
                            Text(completion.subTitle)
                        }
                    }
                    .listRowBackground(Color.clear)
                }
            }
            .listStyle(.plain)
            .scrollContentBackground(.hidden)
        }
        .onChange(of: search) {_,search in
            locationService.update(queryFragment: search)
        }
        .padding()
        .presentationDetents([.height(300)])
        .presentationBackground(.regularMaterial)
        .presentationBackgroundInteraction(.enabled(upThrough: .height(300)))
    }
    
    private func didTapOnCompletion(_ completion: SearchCompletions) {
            Task {
                if let singleLocation = try? await locationService.search(with: "\(completion.title) \(completion.subTitle)").first {
                    searchResults = [singleLocation]
                    mapControl.mapPosition = searchResults.first!.location
                    mapControl.updateMap = true
                }
                activeSheet = .none
            }
        }
}
#Preview {
    SearchBar(activeSheet: .constant(.searchBar), search: .constant("Zarautz"),searchResults:.constant([]))
}
