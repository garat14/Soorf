//
//  MapControl.swift
//  Soorf
//
//  Created by Garat Berasategi on 17/4/24.
//

import Foundation
import SwiftUI
import MapKit

final class MapControl: ObservableObject {
    
    //Initializing Variables for map view
    @Published var mapPosition = CLLocationCoordinate2D(latitude: 21.659805297851562, longitude: -158.05657958984375)
    @Published var beachName = ""
    var defaultSpan = MKCoordinateSpan(latitudeDelta: 0.015, longitudeDelta: 0.015)
    var focusSpan = MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008)
    var updateMap = true
    var centerOnUser = true
}
