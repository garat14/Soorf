//
//  MapView.swift
//  Soorf App
//
//  Created by Garat Berasategi on 7/12/22.
//

import Foundation
import SwiftUI
import MapKit
import UIKit

struct MapView: UIViewRepresentable{
    
    //Enables map update when this value updates
    @EnvironmentObject var mapControl: MapControl
    
    @State var manager = CLLocationManager()
    @Binding var activeSheet: activeSheet?
    @Binding var selection: SearchResult?
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    
    func locationManagerDidChangeAuthorization(){
       print("test")
    }
    
    //Make Map view
    func makeUIView(context: Context) -> MKMapView {
        
        self.manager.requestWhenInUseAuthorization()
        let map = MKMapView()
        map.showsUserLocation = true
        map.delegate = context.coordinator
        map.setUserTrackingMode(MKUserTrackingMode.none, animated: true)
        map.pointOfInterestFilter = MKPointOfInterestFilter.init(including: [MKPointOfInterestCategory.beach,MKPointOfInterestCategory.surfing])
        //center around starting coordinate if no user location is available.
        map.region = MKCoordinateRegion(center:  map.userLocation.coordinate, span: mapControl.defaultSpan)
        map.selectableMapFeatures = .pointsOfInterest
        
        return map
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self, mapcontrol: mapControl)
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        
        //Update view when view is selected
        if mapControl.updateMap {
            let region = MKCoordinateRegion(center: mapControl.mapPosition, span: mapControl.focusSpan)
            uiView.setRegion(region, animated: true)
            mapControl.updateMap = false
        }
    }
    
    class Coordinator: NSObject, MKMapViewDelegate, ObservableObject {
        var myMapView : MapView
        var mapControl: MapControl

        
        init(_ map : MapView, mapcontrol : MapControl)
        {
            self.mapControl = mapcontrol
            self.myMapView = map
        }
        
        func mapView(_ mapView: MKMapView,viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
            return nil
        }

        //Function Called when point of interest is selected
        func mapView(_ mapView: MKMapView, didSelect annotation: MKAnnotation) {
           
            _ = MKCoordinateRegion(center: annotation.coordinate, span: mapControl.defaultSpan)
            myMapView.activeSheet = .weather
            mapControl.mapPosition = annotation.coordinate
            mapControl.beachName = annotation.title!!
            mapControl.updateMap = true
            
            return
        }
            //Necessary to update user location
        func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
            
            if mapControl.centerOnUser {
                let region = MKCoordinateRegion(center: userLocation.coordinate, span: mapControl.defaultSpan)
                
                UIView.animate(withDuration: 0.2)
                {
                    mapView.setRegion(region, animated: true)
                }
                mapControl.centerOnUser = false
            }

        }
    }

    
}




    

