//
//  ContentView.swift
//  Soorf App
//
//  Created by Garat Berasategi on 18/9/22.
//

import SwiftUI
import MapKit
import CoreLocation
import StormGlass
import CoreData
import StoreKit
import SwiftData

enum activeSheet: Identifiable {
    case searchBar, weather, settings
    var id: Int {
        hashValue
    }
}

struct MainView: View {
    @Environment(\.modelContext) var context
    @ObservedObject var mapControl = MapControl()
    
    @Query var appConfigurations: [AppConfigurations]
    @Query var userData: [UserInfo]
    
    @State var activeSheet: activeSheet?
    @State var favoriteViewShowing: Bool = false
    
    @State private var search: String = ""
    @State private var searchResults = [SearchResult]()
    @State private var selectedLocation: SearchResult?
    
    @State var isPro:Bool? = false
    
    //let searchTip = SearchTip()
    //let setFavoriteTip = SetFavoriteTip()
    
    var body: some View{
       //Shows Map View
        NavigationStack{
            ZStack(alignment: .topLeading){
                MapView(activeSheet: $activeSheet, selection: $selectedLocation)
                    .environmentObject(mapControl)
                    .onAppear{
                        //Prepare SwiftData
                        if appConfigurations.isEmpty {
                            let appconfig = AppConfigurations()
                            context.insert(appconfig)
                        }
                        if userData.isEmpty {
                            let data = UserInfo()
                            context.insert(data)
                        }
                    }
                    .ignoresSafeArea()
                //Show tip when first time seeing app and whenever button is pressed
                    .overlay(content: {
                        if (userData.isEmpty || userData[0].isTipShowing == true)  {
                            Tutorial_View()
                        }
                    })
                    .onChange(of: searchResults) {
                        if let firstResult = searchResults.first, searchResults.count == 1 {
                            selectedLocation = firstResult
                        }
                    }
                if (favoriteViewShowing){
                    SideMenu(isShowing: $favoriteViewShowing)
                        .environmentObject(mapControl)
                }
            }
            .toolbarBackground(.visible)
            .toolbarTitleDisplayMode(.inline)
            .toolbar{
                ToolbarItemGroup(placement: .topBarLeading){
                    Button{
                        favoriteViewShowing = false
                        userData[0].isTipShowing = false
                        activeSheet = .settings
                    }
                    label:{
                        Image(systemName: "gearshape")
                            .fontWeight(.bold)
                            .foregroundStyle(Color("Main").opacity(0.8))
                    }
                    .clipShape(Circle())
                    .tint(Color(.systemBackground).opacity(0.8))
                    Button{
                        activeSheet = .none
                        favoriteViewShowing = false
                        userData[0].isTipShowing.toggle()
                    }
                    label:{
                        Image(systemName: "info.circle")
                            .fontWeight(.bold)
                            .foregroundStyle(Color("Main").opacity(0.8))
                    }
                    .clipShape(Circle())
                    .tint(Color(.systemBackground).opacity(0.8))
                }
                ToolbarItemGroup(placement: .topBarTrailing){
                    Button{
                        favoriteViewShowing.toggle()
                        userData[0].isTipShowing = false
                        activeSheet = .none
                    }
                    label:{
                        Image(systemName: "star.fill")
                            .fontWeight(.bold)
                            .foregroundStyle(Color("Main").opacity(0.8))
                    }
                    //.popoverTip(setFavoriteTip)
                    .clipShape(Circle())
                    .tint(Color(.systemBackground).opacity(0.8))
                    Button{
                        favoriteViewShowing = false
                        userData[0].isTipShowing = false
                        activeSheet = .searchBar
                        //Task{await SearchTip.searchViewVisited.donate()}
                        //Task{await SetFavoriteTip.favoriteViewVisited.donate()}
                    }
                    label:{
                        Image(systemName: "magnifyingglass")
                            .fontWeight(.bold)
                            .foregroundStyle(Color("Main").opacity(0.8))
                    }
                    //.popoverTip(searchTip)
                    .clipShape(Circle())
                    .tint(Color(.systemBackground).opacity(0.8))
                }
            }
        }
        .sheet(item: $activeSheet){ item in
           switch item {
           case .searchBar:
               SearchBar(activeSheet: $activeSheet, search: $search, searchResults: $searchResults)
                   .environmentObject(mapControl)
           case .weather:
               SubscriptionCheck(userIsSubscribed: $isPro)
                   .environmentObject(mapControl)
                   .presentationDragIndicator(.visible)
           case .settings:
               Settings(selectedTempUnit: appConfigurations[0].tempUnit,
                        selectedLengthUnit: appConfigurations[0].lengthUnit,
                        selectionWave: CGFloat(appConfigurations[0].minWave)...CGFloat(appConfigurations[0].maxWave),
                        selectionSwell: CGFloat(appConfigurations[0].minSwell)...CGFloat(appConfigurations[0].maxSwell),
                        selectionWind: CGFloat(appConfigurations[0].minWind)...CGFloat(appConfigurations[0].maxWind))
               .presentationDragIndicator(.visible)
               .navigationTitle("Settings")
           }
       }
        .subscriptionStatusTask(for: "21346008"){ taskState in
           if let value = taskState.value {
               isPro = !value
                   .filter { $0.state != .revoked && $0.state != .expired }
                   .isEmpty
           } else {
               isPro = false
        }
           
       }
    }
}



#Preview {
    MainView()
}


