//
//  MapKit.swift
//  Soorf
//
//  Created by Garat Berasategi on 21/12/23.
//

import SwiftUI
import MapKit

struct MapKit: View {
    @available(iOS 17.0, *)
    var body: some View {
            Map()
    }
}

#Preview {
    MapKit()
}
