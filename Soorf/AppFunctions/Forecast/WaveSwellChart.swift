//
//  ConditionChart.swift
//  Soorf
//
//  Created by Garat Berasategi on 21/10/24.
//

import SwiftUI
import Charts


struct WaveSwellChart: View {
    
    @State var selectedIndex: String?
    
    var dailyData: [weatherData]
    var selectedDate: String
    @State private var detailShowing: Bool = true
    
    @Binding var selectedTime: String
    
    var body: some View {
        GroupBox("Wave & Swell"){
            Chart{
                ForEach(dailyData, id: \.id){data in
                    LineMark(
                        x: .value("",data.time),
                        y: .value("Tide",data.waveHeight),
                        series: .value("","Wave"))
                    .foregroundStyle(Color("Dark").gradient)
                    .lineStyle(StrokeStyle(lineWidth: 3))
                    .interpolationMethod(.cardinal)
                    .lineStyle(by: .value("Type", "Wave"))
                    
                    LineMark(
                        x: .value("",data.time),
                        y: .value("Tide",data.swellHeight),
                        series: .value("","Swell"))
                    .foregroundStyle(Color("Light").gradient)
                    .lineStyle(StrokeStyle(lineWidth: 2,dash:[2]))
                    .interpolationMethod(.cardinal)
                    .lineStyle(by: .value("Type", "Swell"))
                    //selectedIndex
                    if selectedTime == data.time || (selectedIndex == data.time && selectedTime == "") {
                        RectangleMark(
                            x: .value("Index", data.time),
                            yStart: .value("Value", 0),
                            yEnd: .value("Value", (dailyData.map(\.waveHeight).max() ?? data.waveHeight) + 1),
                            width: 1
                        )
                        .foregroundStyle(Color("Light").opacity(0.7))
                        PointMark(
                            x: .value("Index", data.time),
                            y: .value("Value", data.waveHeight))
                        .foregroundStyle(Color("Dark"))
                        .symbolSize(100)
                        PointMark(
                            x: .value("Index", data.time),
                            y: .value("Value", data.swellHeight)
                        )
                        .foregroundStyle(Color("Light"))
                        .symbolSize(100)
                        PointMark(
                            x: .value("Index", data.time),
                            y: .value("Value", data.waveHeight))
                        .foregroundStyle(Color.white)
                        .symbolSize(50)
                        PointMark(
                            x: .value("Index", data.time),
                            y: .value("Value", data.swellHeight))
                        .foregroundStyle(Color.white)
                        .symbolSize(50)
                    }
                }
            }
            .chartOverlay { chart in
                        GeometryReader { geometry in
                            Rectangle()
                                .fill(Color.clear)
                                .contentShape(Rectangle())
                                .gesture(
                                    DragGesture()
                                        .onChanged { value in
                                            detailShowing = true
                                            let currentX = value.location.x - geometry[chart.plotFrame!].origin.x
                                            guard currentX >= 0, currentX < chart.plotSize.width else {
                                                return
                                            }
                                            
                                            guard let index = chart.value(atX: currentX, as: String.self) else {
                                                return
                                            }
                                            selectedIndex = index
                                            selectedTime = index
                                        }
                                )
                        }
                    }
            .chartXAxis {
                AxisMarks(values: dailyData.map{$0.time}) {test in
                    if (test.index == 6 || test.index == 12 || test.index == 18){
                        AxisGridLine()
                        AxisValueLabel()
                    }
                }
            }
            .chartYAxis{
                //AxisMarks(stroke: StrokeStyle(lineWidth: 0))
            }
        }
        .backgroundStyle(Color("Light").opacity(0.1))
    }
}

/*#Preview {
    ConditionChart(dailyData: dailyData)
}*/
