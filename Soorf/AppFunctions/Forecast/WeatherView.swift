//
//  WeatherView.swift
//  Soorf App
//
//  Created by Garat Berasategi on 29/9/22.
//

import StormGlass
import SwiftUI
import MapKit
import Charts
import SwiftData

struct WeatherView: View {
    @Environment(\.modelContext) var context
    @EnvironmentObject var mapControl: MapControl

    @Query(sort: \FavoriteBeach.beachName) var favoriteBeaches: [FavoriteBeach] = []
    @Query var appConfiguration: [AppConfigurations]
    
    @ObservedObject var fetchDataCall = fetchData()
    
    @State var currentActiveItem : tideData?
    
    @State var currentTab: String = "24h"
    @State var selectedDate: String = ""
    
    @State var favoriteSet: Bool = false
    @State var selectedTime: String = ""
    //let searchTip = SearchTip()
    
    var body: some View {
        ZStack{
            NavigationStack{
                List{
                    HStack{
                        GroupBox{
                            VStack{
                                HStack{
                                    Image(systemName: "sunrise")
                                        .bold()
                                    Spacer()
                                    Text (fetchDataCall.fetchedAstronomyData.sunrise)
                                        .bold()
                                }
                                Spacer()
                                HStack{
                                    Image(systemName: "sunset")
                                        .bold()
                                    Spacer()
                                    Text (fetchDataCall.fetchedAstronomyData.sunset)
                                        .bold()
                                }
                            }
                            .frame(width: 130, height: 50)
                        }
                        .backgroundStyle(Color("Light").opacity(0.1))
                        Spacer()
                        GroupBox{
                            VStack{
                                HStack{
                                    Image(systemName: "thermometer.medium")
                                        .padding(EdgeInsets(top: 0, leading: 4, bottom: 0, trailing: 0))
                                        .bold()
                                    Spacer()
                                    if(fetchDataCall.fetchedData.count > 0){
                                        getCorrectTempUnit(temp: (fetchDataCall.fetchedData[0].airTemperature)!, unit: appConfiguration[0].tempUnit)
                                        .bold()}
                                }
                                Spacer()
                                HStack{
                                    Image(systemName: "thermometer.and.liquid.waves")
                                        .frame(width: 30,alignment: .leading)
                                        .bold()
                                    Spacer()
                                    if(fetchDataCall.fetchedData.count > 0){
                                        getCorrectTempUnit(temp: (fetchDataCall.fetchedData[0].waterTemperature)!, unit: appConfiguration[0].tempUnit)
                                        .bold()
                                    }
                                }
                            }
                            .frame(width: 130, height: 50)
                        }
                        .backgroundStyle(Color("Light").opacity(0.1))
                    }
                    .listRowSeparator(.hidden)
                    if fetchDataCall.showingData.count > 0 {
                        VStack{
                            HStack{
                                ConditionWaveView(conditionData: fetchDataCall.showingData,
                                                  title: "Wave",
                                                  minCondition: appConfiguration[0].minWave,
                                                  maxCondition: appConfiguration[0].maxWave,
                                                  time: fetchDataCall.fetchedCurrentTime.currentTimeString)
                                //.frame(height: 100)
                                Spacer()
                                ConditionSwellView(conditionData: fetchDataCall.showingData,
                                                   title: "Swell",
                                                   minCondition: appConfiguration[0].minSwell,
                                                   maxCondition: appConfiguration[0].maxSwell,
                                                   time: fetchDataCall.fetchedCurrentTime.currentTimeString)
                                //.frame(height: 100)
                            }
                            //.padding(EdgeInsets(top: 10, leading: 0, bottom: 20, trailing: 0))
                            HStack{
                                ConditionWindView(conditionData: fetchDataCall.showingData,
                                                  title: "Wind",
                                                  minCondition: appConfiguration[0].minWind,
                                                  maxCondition: appConfiguration[0].maxWind,
                                                  time:fetchDataCall.fetchedCurrentTime.currentTimeString)
                                //.frame(height: 80)
                                Spacer()
                                ConditionWindView(conditionData: fetchDataCall.showingData,
                                                  title: "Wind",
                                                  minCondition: appConfiguration[0].minWind,
                                                  maxCondition: appConfiguration[0].maxWind,
                                                  time:fetchDataCall.fetchedCurrentTime.currentTimeString)
                                .hidden()
                                //.frame(height: 80)
                            }
                            //.padding(EdgeInsets(top: 20, leading: 0, bottom: 20, trailing: 0))
                        }
                    }
                    if fetchDataCall.fetchedWeeklyData.count > 6 {
                        HStack{
                            PickerView(selectedDate: $selectedDate, dates: fetchDataCall.followingDates)
                            Text(String(handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)) + ":00")
                                .foregroundStyle(Color("Dark"))
                        }.bold()
                        .listRowSeparator(.hidden)
                        GroupBox{
                            VStack(alignment: .leading){
                                HStack{
                                    Text("Wave:").bold()
                                    getCorrectSizeUnit(size: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].waveHeight, unit: appConfiguration[0].lengthUnit).bold()
                                    Spacer()
                                    Image(systemName: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].waveDirectionIcon).font(.footnote)
                                    Text(getDirection(direction: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].waveDirectionIcon) + "º").font(.footnote)
                                    Image(systemName: "timer").font(.footnote)
                                    Text(String(format: "%.1f", fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].wavePeriod) + "s").font(.footnote)
                                }
                                HStack{
                                    Text("Swell:").bold()
                                    getCorrectSizeUnit(size: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].swellHeight, unit: appConfiguration[0].lengthUnit).bold()
                                    Spacer()
                                    Image(systemName: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].swellDirectionIcon).font(.footnote)
                                    Text(getDirection(direction: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].swellDirectionIcon) + "º").font(.footnote)
                                    Image(systemName: "timer").font(.footnote)
                                    Text(String(format: "%.1f", fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].swellPeriod) + "s").font(.footnote)
                                }
                                HStack{
                                    Text("Wind:").bold()
                                    getCorrectSpeedUnit(size: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].windSpeed, unit: appConfiguration[0].lengthUnit).bold()
                                    Spacer()
                                    Image(systemName: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].waveDirectionIcon).font(.footnote)
                                    Text(getDirection(direction: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].windDirectionIcon) + "º").font(.footnote)
                                }
                                HStack{
                                    Text("Tide:").bold()
                                    getCorrectSizeUnit(size: fetchDataCall.fetchedWeeklyTideData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)][handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString)].tide, unit: appConfiguration[0].lengthUnit).bold()
                                    Spacer()
                                    Image(systemName: tideDirectionIcon(tide: fetchDataCall.fetchedWeeklyTideData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)],index: handleTimeIndex(selected: selectedTime, original: fetchDataCall.fetchedCurrentTime.currentTimeString))).font(.footnote)
                                    
                                }
                           }
                        }
                        .backgroundStyle(Color("Light").opacity(0.1))
                        WaveSwellChart(selectedIndex: fetchDataCall.fetchedCurrentTime.currentTimeString, dailyData: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)], selectedDate: selectedDate, selectedTime: $selectedTime)
                            .listRowSeparator(.hidden)
                        WindChart(selectedIndex: fetchDataCall.fetchedCurrentTime.currentTimeString, dailyData: fetchDataCall.fetchedWeeklyData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)], selectedDate: selectedDate, selectedTime: $selectedTime)
                            .listRowSeparator(.hidden)
                        TideChart(selectedIndex: fetchDataCall.fetchedCurrentTime.currentTimeString, dailyData: fetchDataCall.fetchedWeeklyTideData[selectDayOfForecast(dayString: selectedDate, weeklyData:fetchDataCall.fetchedWeeklyData)], selectedDate: selectedDate, selectedTime: $selectedTime)
                    }
                }
                .navigationTitle(mapControl.beachName)
                .navigationBarTitleDisplayMode(.large)
                .toolbar{
                    ToolbarItem(placement: .topBarTrailing){
                        Button{
                            favoriteSet.toggle()
                        }
                    label:{
                        if favoriteSet == true {
                            Image(systemName: "star.fill")
                                .fontWeight(.bold)
                                .tint(.yellow)
                        }
                        else{
                            Image(systemName: "star")
                                .fontWeight(.bold)
                                .tint(.gray)
                        }
                    }
                    .clipShape(Circle())
                    .tint(Color(.systemBackground).opacity(0.8))
                    }
                }
            }
            if fetchDataCall.isLoading{
                LoadingView()
                    .onDisappear{
                        if (fetchDataCall.followingDates.count > 0 && selectedDate == ""){
                             selectedDate = fetchDataCall.followingDates[0].date
                         }
                    }
            }
            else if (fetchDataCall.APIerror) {
                APIErrorHandling()
            }
        }
        .onAppear{
            favoriteSet = isBeachFavorite(savedBeaches: favoriteBeaches, beachName: mapControl.beachName)
            //API call on view appear
            fetchDataCall.callAPI(beachCoordinate: mapControl.mapPosition)
            
        }
        .onDisappear{
            if favoriteSet == true{
                let favoriteBeach = FavoriteBeach(beachName: mapControl.beachName, latitude: Float(mapControl.mapPosition.latitude), longitude: Float(mapControl.mapPosition.longitude))
                context.insert(favoriteBeach)
            }
            else if favoriteSet == false && isBeachFavorite(savedBeaches: favoriteBeaches, beachName: mapControl.beachName) == true{
                for beach in favoriteBeaches {
                    if beach.beachName == mapControl.beachName {
                        context.delete(beach)
                    }
                }
            }
        }
    }
}

struct PickerView: View {
    @Binding var selectedDate: String
    var dates: [nextDates]

    var body: some View {
        Picker("", selection: $selectedDate,
                content: {
            ForEach(0..<dates.count, id: \.self) {day in
                Text(self.dates[day].date)
                    .foregroundStyle(Color.black)
                    .bold()
                    .tag(self.dates[day].date)
                }
        })
        .tint(Color("Dark"))
        .labelsHidden()
        .pickerStyle(.menu)
        .frame(height: 50)
    }
}

struct LoadingView: View {
    var body: some View {
        ZStack{
            Color(.systemBackground)
                .ignoresSafeArea()
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: .cyan))
                .scaleEffect(1.5)
        }
    }
}

///Selects index of all forecast values so
func selectDayOfForecast (dayString: String, weeklyData: [[weatherData]]) -> Int {
    var kont: Int = 0
    if dayString != "" {
        
        let oldDateArr = dayString.components(separatedBy: " ")
        let newDate = oldDateArr[1] + " " + oldDateArr[0]
        
        for day in weeklyData {
            if (day[0].date == newDate) {
                return kont
            }
            kont += 1
        }
    }
    
    return 0
}

func getCorrectTempUnit (temp: Int, unit: tempUnit) -> Text {
    
    if unit == .farenheit{
        let Ftemp:Int = ((temp * 5)/9) + 32
        return Text(String(Ftemp) + "ºF")
    }
    
    return Text(String(temp) + "ºC")
}

func getCorrectSizeUnit (size: Double, unit: lengthUnit) -> Text {
    
    if unit == .feet{
        let unit:Double = size * 3.28084
        return Text(String(format: "%.1f", unit) + "ft")
    }
    
    return Text(String(format: "%.1f",size) + "m")
}

func getCorrectSpeedUnit (size: Double, unit: lengthUnit) -> Text {
    if unit == .feet{
        let Ftemp:Double = size * 3.28084
        return Text(String(format: "%.1f",Ftemp) + "ft/s")
    }
    
    return Text(String(format: "%.1f",size) + "m/s")
}

func isBeachFavorite(savedBeaches: [FavoriteBeach],beachName: String) -> Bool {

    var isfavorite = false
    
    savedBeaches.forEach{beach in
        if beach.beachName == beachName {
            isfavorite = true
        }
    }

    return isfavorite
}

/// Handles the error that the API generates 24h == 00 date in  index 23
func handleTimeIndex(selected: String ,original: String) -> Int {
    
    if selected == "" {
        return Int(original)!
    }

    return Int(selected)!
}

func tideDirectionIcon(tide: [tideData], index: Int) -> String {
    
    if (index == 23){
        if (tide[index-1].tide > tide[index].tide){
            return "water.waves.and.arrow.down"
        }
        else{
            return "water.waves.and.arrow.up"
        }
    }
    else {
        if (tide[index].tide < tide[index+1].tide){
            return "water.waves.and.arrow.up"
        }
        else{
            return "water.waves.and.arrow.down"
        }
    }
    
    return "water.waves"
}
