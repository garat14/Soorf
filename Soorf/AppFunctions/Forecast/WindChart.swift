//
//  WindChart.swift
//  Soorf
//
//  Created by Garat Berasategi on 3/1/25.
//

import SwiftUI
import Charts

struct WindChart: View {
    @State var selectedIndex: String?
    
    var dailyData: [weatherData]
    var selectedDate: String
    @State private var detailShowing: Bool = true
    
    @Binding var selectedTime: String
    
    var body: some View{
        GroupBox("Wind"){
            Chart{
                ForEach(dailyData, id: \.id){data in
                    if selectedTime == data.time {
                        BarMark(
                            x: .value("",data.time),
                            y: .value("Tide",data.windSpeed))
                        .foregroundStyle(Color("Dark").gradient)
                        .annotation{
                            Image(systemName: data.windDirectionIcon)
                                .font(.caption)
                                .bold()
                                .foregroundStyle(Color("Dark"))
                        }
                    }
                    else{
                        BarMark(
                            x: .value("",data.time),
                            y: .value("Tide",data.windSpeed))
                        .foregroundStyle(Color("Dark").opacity(0.3).gradient)
                        .annotation{
                            Image(systemName: data.windDirectionIcon)
                                .font(.caption)
                                .bold()
                                .foregroundStyle(Color("Dark").opacity(0.3))
                        }
                    }
                }
            }
            .onAppear{
                selectedTime = selectedIndex!
            }
            .frame(height: 100)
            .chartOverlay { chart in
                        GeometryReader { geometry in
                            Rectangle()
                                .fill(Color.clear)
                                .contentShape(Rectangle())
                                .gesture(
                                    DragGesture()
                                        .onChanged { value in
                                            detailShowing = true
                                            let currentX = value.location.x - geometry[chart.plotFrame!].origin.x
                                            guard currentX >= 0, currentX < chart.plotSize.width else {
                                                return
                                            }
                                            
                                            guard let index = chart.value(atX: currentX, as: String.self) else {
                                                return
                                            }
                                            selectedIndex = index
                                            selectedTime = index
                                        }
                                )
                        }
                    }
            .chartXAxis {
                AxisMarks(values: dailyData.map{$0.time}) {test in
                    if (test.index == 6 || test.index == 12 || test.index == 18){
                        AxisGridLine()
                        AxisValueLabel()
                    }
                }
            }
            .chartYAxis{}
        }
        .backgroundStyle(Color("Light").opacity(0.1))
    }
}

//#Preview {
//    WindChart()
//}
