//  TideChart.swift
//  Soorf
//
//  Created by Garat Berasategi on 19/6/23.
//

import Foundation
import Charts
import SwiftUI

struct TideChart: View {
    
    @State var selectedIndex: String?
    
    var dailyData: [tideData]
    var selectedDate: String
    @State private var detailShowing: Bool = true
    
    @Binding var selectedTime: String
    
    var body: some View{
        GroupBox("Tide"){
            Chart{
                ForEach(dailyData, id: \.id){data in
                    LineMark(
                        x: .value("",data.time),
                        y: .value("Tide",data.tide))
                    .foregroundStyle(Color("Dark").opacity(0.3).gradient)
                    .lineStyle(StrokeStyle(lineWidth: 3))
                    .interpolationMethod(.cardinal)
                    if selectedTime == data.time {
                        RectangleMark(
                            x: .value("Index", data.time),
                            yStart: .value("Value", dailyData.map(\.tide).min()!),
                            yEnd: .value("Value", dailyData.map(\.tide).max()!),
                            width: 1
                        )
                        .foregroundStyle(Color("Light").opacity(0.7))
                        PointMark(
                            x: .value("Index", data.time),
                            y: .value("Value", data.tide))
                        .foregroundStyle(Color("Dark"))
                        .symbolSize(100)
                        
                        PointMark(
                            x: .value("Index", data.time),
                            y: .value("Value", data.tide))
                        .foregroundStyle(Color.white)
                        .symbolSize(50)
                    }
                }
            }
            .onAppear{
                selectedTime = selectedIndex!
            }
            .frame(height: 100)
            .chartOverlay { chart in
                        GeometryReader { geometry in
                            Rectangle()
                                .fill(Color.clear)
                                .contentShape(Rectangle())
                                .gesture(
                                    DragGesture()
                                        .onChanged { value in
                                            detailShowing = true
                                            let currentX = value.location.x - geometry[chart.plotFrame!].origin.x
                                            guard currentX >= 0, currentX < chart.plotSize.width else {
                                                return
                                            }
                                            
                                            guard let index = chart.value(atX: currentX, as: String.self) else {
                                                return
                                            }
                                            selectedIndex = index
                                            selectedTime = index
                                        }
                                )
                        }
                    }
            .chartXAxis {
                AxisMarks(values: dailyData.map{$0.time}) {test in
                    if (test.index == 6 || test.index == 12 || test.index == 18){
                        AxisGridLine()
                        AxisValueLabel()
                    }
                }
            }
            .chartYAxis{
                AxisMarks(values: [0])
                {
                    AxisGridLine()
                    AxisValueLabel()
                }
            }
        }
        .backgroundStyle(Color("Light").opacity(0.1))
    }
}
