//
//  TimezoneAPI.swift
//  Soorf
//
//  Created by Garat Berasategi on 7/1/25.
//

import Foundation
import SwiftUI

func getTimeZone(lat: Double, long: Double) -> String{
    var timezoneName: String = ""
    let sLat = String(format: "%f", lat)
    let sLong = String(format: "%f", long)
    var test = ""
    
    var request = URLRequest(url: URL(string: "https://api.ipgeolocation.io/timezone?apiKey=d8f33aaaa0b64e8285370fb474078557&lat="+sLat+"&long="+sLong+"&fields=timezone")!,timeoutInterval: Double.infinity)
    request.httpMethod = "GET"
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data else {
            print(String(describing: error))
            return
        }
        test = String(data: data, encoding: .utf8)!
        let pairs = test.split(separator: ",")
        let final = pairs[0].split(separator: ":")
        var clean = String(final[1])
        clean = String(clean.dropLast())
        timezoneName = String(clean.dropFirst())
        
    }
    task.resume()
    
    repeat {
        RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
    } while timezoneName == ""
    
    return timezoneName
    
    
}
