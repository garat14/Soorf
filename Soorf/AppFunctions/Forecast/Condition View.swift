//
//  Condition View.swift
//  Soorf
//
//  Created by Garat Berasategi on 13/5/24.
//

import SwiftUI
import Charts
import SwiftData


struct ConditionWaveView: View {
    
    @Query var appConfiguration: [AppConfigurations]
    
    var conditionData: [weatherData]
    var title: String
    var minCondition: Float
    var maxCondition: Float
    var time: String
    
    var body: some View {
        GroupBox{
            VStack (alignment: .leading){
                HStack{
                    Text(title)
                        .font(.body)
                        .bold()
                        Image("wave")
                            .resizable()
                            .frame(width: 17,height: 20)
                        Spacer()
                        getCorrectSizeUnit(size: Double(Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].waveHeight)), unit: appConfiguration[0].lengthUnit)
                            .font(.subheadline)
                }
                RoundedRectangle(cornerRadius: 5)
                    .fill(getColor(min: minCondition,max: maxCondition, measurement: Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].waveHeight)).opacity(0.8))
                    .frame(height: 10)
                HStack{

                    Image(systemName: conditionData[selectHourIndex(hourString: time, dayData: conditionData)].waveDirectionIcon)
                        .resizable()
                        .frame(width: 13,height: 15)
                    Text(getDirection(direction: conditionData[selectHourIndex(hourString: time, dayData: conditionData)].waveDirectionIcon) + "º")
                        .font(.callout)
                    Spacer()
                    Image(systemName: "timer")
                        .resizable()
                        .frame(width: 13,height: 15)
                    Text(String(format: "%.1f",conditionData[selectHourIndex(hourString: time, dayData: conditionData)].wavePeriod) + "s")
                        .font(.subheadline)
                }
            }
        }
        .backgroundStyle(getColor(min: minCondition,max: maxCondition, measurement: Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].waveHeight)).gradient.opacity(0.1))
    }
}

struct ConditionSwellView: View {
    
    @Query var appConfiguration: [AppConfigurations]
    
    var conditionData: [weatherData]
    var title: String
    var minCondition: Float
    var maxCondition: Float
    var time: String
    
    var body: some View {
        GroupBox{
            VStack (alignment: .leading){
                HStack{
                    Text(title)
                        .font(.body)
                        .bold()
                        Image(systemName: "water.waves")
                            .resizable()
                            .frame(width: 15,height: 15)
                        Spacer()
                        getCorrectSizeUnit(size: Double(Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].swellHeight)), unit: appConfiguration[0].lengthUnit)
                            .font(.subheadline)
                }
                RoundedRectangle(cornerRadius: 5)
                    .fill(getColor(min: minCondition,max: maxCondition, measurement: Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].swellHeight)).opacity(0.8))
                    .frame(height: 10)
                HStack{

                    Image(systemName: conditionData[selectHourIndex(hourString: time, dayData: conditionData)].swellDirectionIcon)
                        .resizable()
                        .frame(width: 13,height: 15)
                    Text(getDirection(direction: conditionData[selectHourIndex(hourString: time, dayData: conditionData)].swellDirectionIcon) + "º")
                        .font(.callout)
                    Spacer()
                    Image(systemName: "timer")
                        .resizable()
                        .frame(width: 13,height: 15)
                    Text(String(format: "%.1f",conditionData[selectHourIndex(hourString: time, dayData: conditionData)].swellPeriod) + "s")
                        .font(.subheadline)
                }
            }
        }
        .backgroundStyle(getColor(min: minCondition,max: maxCondition, measurement: Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].swellHeight)).gradient.opacity(0.1))
    }
}

struct ConditionWindView: View {
    
    @Query var appConfiguration: [AppConfigurations]
    
    var conditionData: [weatherData]
    var title: String
    var minCondition: Float
    var maxCondition: Float
    var time: String
    
    var body: some View {
        GroupBox{
            VStack (alignment: .leading){
                HStack{
                    Text(title)
                        .font(.body)
                        .bold()
                    Image(systemName: "wind")
                    Spacer()
                }
                RoundedRectangle(cornerRadius: 5)
                    .fill(getColor(min: minCondition,max: maxCondition, measurement: Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].windSpeed)).opacity(0.8))
                    .frame(height: 10)
                HStack{
                    Image(systemName: conditionData[selectHourIndex(hourString: time, dayData: conditionData)].windDirectionIcon)
                    Text(getDirection(direction: conditionData[selectHourIndex(hourString: time, dayData: conditionData)].windDirectionIcon) + "º")
                        .font(.subheadline)
                    Spacer()
                    getCorrectSpeedUnit(size: Double(Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].windSpeed)), unit: appConfiguration[0].lengthUnit)
                        .font(.subheadline)
                }
            }
        }
        .backgroundStyle(getColor(min: minCondition,max: maxCondition, measurement: Float(conditionData[selectHourIndex(hourString: time, dayData: conditionData)].windSpeed)).gradient.opacity(0.1))
    }
}

func getDirection(direction: String)-> String {
    
    var rDirection: String = "N"
    
    switch direction {
    case "arrow.up":
        rDirection = "N"
        break
    case "arrow.up.right":
        rDirection = "NE"
        break
    case "arrow.right":
        rDirection = "E"
        break
    case "arrow.down.right":
        rDirection = "SE"
        break
    case "arrow.down":
        rDirection = "S"
        break
    case "arrow.down.left":
        rDirection = "SW"
        break
    case "arrow.left":
        rDirection = "W"
        break
    case "arrow.up.left":
        rDirection = "NW"
        break
    default:
        rDirection = "N"
    }
    return rDirection
    
}

func getColor(min: Float, max: Float, measurement: Float) -> Color {
    if measurement < min {
        return Color(.yellow)
    }
    if measurement > max {
        return Color(.red)
    }
    return Color(.green)
}

func selectHourIndex (hourString: String, dayData: [weatherData]) -> Int {
    var kont: Int = 0
    if (hourString != ""){
        for hour in dayData {
            if (hour.time == hourString){
                return kont
            }
            kont += 1
            
        }
    }
    
    return 0
}

/*
#Preview {
    List{
        HStack{
            Condition_View(title: "Wave", size: 0.63, period: 0.4, arrow: "arrow.up.right", minCondition: 1, maxCondition: 2)
            Wind_View(title: "Wind", size: 0.63, arrow: "arrow.up.right", minCondition: 1, maxCondition: 2)
        }
        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
    }
}*/
