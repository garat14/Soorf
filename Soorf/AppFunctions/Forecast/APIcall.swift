//
//  APIcall.swift
//  Soorf App
//
//  Created by Garat Berasategi on 12/4/23.
//

import Foundation
import StormGlass
import SwiftUI
import MapKit
import CoreData

struct weatherData{
    let id = UUID()
    var time: String
    var date: String
    var airTemperature: Int?
    var waterTemperature: Int?
    var waveHeight: Double
    var waveDirection: Double
    var waveDirectionIcon: String
    var wavePeriod: Double
    var swellHeight: Double
    var swellPeriod: Double
    var swellDirection: Double
    var swellDirectionIcon: String
    var windSpeed: Double
    var windDirection: Double
    var windDirectionIcon: String
}

struct astronomyData{
    let id = UUID()
    var sunrise: String
    var sunset: String
   }

struct tideData{
    let id = UUID()
    var time: String
    var tide: Double
   }

struct nextDates: Hashable{
    let id = UUID()
    var date: String
}

struct currentTime: Hashable {
    let id = UUID()
    var currentTimeString: String
}

class fetchData: NSObject, ObservableObject {
    
    @Published var isLoading = false
    @Published var APIerror = false
    @Published var exceededAllowedCalls = false
    @Published var fetchedData : [weatherData] = []
    @Published var followingDates : [nextDates] = []
    @Published var tempData : [weatherData] = []
    @Published var fetchedWeeklyData = [[weatherData]]()
    @Published var fetchedAstronomyData: astronomyData = astronomyData(sunrise: "",sunset: "")
    @Published var fetchedTideData : [tideData] = []
    @Published var fetchedWeeklyTideData = [[tideData]]()
    @Published var showingData: [weatherData] = []
    @Published var fetchedCurrentTime = currentTime(currentTimeString: "")
    
    private var kont2 = 0
    func callAPI(beachCoordinate: CLLocationCoordinate2D) {

        
        self.isLoading = true
        
        //API key to access data
        SGConfiguration.shared.apiKey = "a37d2204-d7d3-11ed-bc36-0242ac130002-a37d22d6-d7d3-11ed-bc36-0242ac130002"
        
        //Coordinate of interested data point
        let requestedCoord = SGCoordinate.init(latitude: beachCoordinate.latitude , longitude: beachCoordinate.longitude)
        
        //Interested data for app
        let requestedParams = [SGWeatherPointParameters.swellHeight,SGWeatherPointParameters.airTemperature,SGWeatherPointParameters.swellPeriod,SGWeatherPointParameters.swellDirection,SGWeatherPointParameters.waveHeight,SGWeatherPointParameters.waveDirection,SGWeatherPointParameters.wavePeriod,SGWeatherPointParameters.windSpeed,SGWeatherPointParameters.windDirection,SGWeatherPointParameters.waterTemperature]
        
        //Change Timezone
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm YY/MM/DD"
        
        //Get Time Zone of Selected Beach
        let TZ = getTimeZone(lat: beachCoordinate.latitude, long: beachCoordinate.longitude)
        formatter.timeZone = TimeZone(identifier: TZ)
        let editedDate = formatter.string(from: Date())
        let dateFormatter = DateFormatter()
        let date = dateFormatter.date(from: "00:00 " + editedDate.components(separatedBy: " ")[1])
        
        //Prepare calls
        
        let weatherEndpoint = SGWeatherPointRequest(coordinate: requestedCoord,values: requestedParams, startDate: date)
        let astronomyEndpoint = SGAstronomyPointRequest(coordinate: requestedCoord)
        let tideEndpoint = SGTideSeaLevelPointRequest(coordinate: requestedCoord,startDate: date)
        
        //Execute API call
        let weatherRequest = SGRequest(endpoint: weatherEndpoint)
        let astronomyRequest = SGRequest(endpoint: astronomyEndpoint)
        let tideRequest = SGRequest(endpoint: tideEndpoint)

        //Fetch Astronomy information
        astronomyRequest.fetch{
            _astronomyData in
            do
            {
                let astronomyData = try _astronomyData.get()
                //If there are more than 3 calls available continue with process (Every tap equals 3 taps)
                if (astronomyData.meta.dailyQuota - astronomyData.meta.requestCount) >= 3{
                    //Change Date to String with apropriate TimeZone
                    let sunrise = astronomyData.data.first?.sunrise!
                    let sunset = astronomyData.data.first?.sunset!
                    let sunFormatter = DateFormatter()
                    sunFormatter.dateFormat = "HH:mm"
                    //Get Time Zone of Selected Beach
                    sunFormatter.timeZone = TimeZone(identifier: TZ)
                    self.fetchedAstronomyData.sunrise = sunFormatter.string(from: sunrise!)
                    self.fetchedAstronomyData.sunset = sunFormatter.string(from: sunset!)
                }
                //Handle call limits
                else {
                    self.exceededAllowedCalls = true
                }
                
            }
            catch {
                self.APIerror = true
            }
        }
            
        if self.APIerror == false {
            
            weatherRequest.fetch{ [self]
                _weatherData in
                do
                {
                    let _hours = try _weatherData.get().hours
                    //Get hourly data information
                    for _hour in _hours
                    {
                        //Prepare time string for display
                        let total_time = simplifyTime(date: _hour.time)
                        
                        //Prepare date for display
                        let currentDate = simplifyDate(date: _hour.time)
                        
                        self.fetchedData.append(weatherData(
                            time: total_time,
                            date: currentDate,
                            airTemperature: Int((_hour.airTemperature?.values.first!.value.rounded())!),
                            waterTemperature: Int((_hour.waterTemperature?.values.first!.value.rounded())!),
                            waveHeight: (_hour.waveHeight?.values.first?.value)!,
                            waveDirection: (_hour.waveDirection?.values.first?.value)!,
                            waveDirectionIcon: self.getDirectionArrow(direction: (_hour.waveDirection?.values.first?.value)!),
                            wavePeriod: (_hour.wavePeriod?.values.first!.value)!,
                            swellHeight: (_hour.swellHeight?.values.first!.value)!,
                            swellPeriod: (_hour.swellPeriod?.values.first!.value)!,
                            swellDirection: (_hour.swellDirection?.values.first!.value)!,
                            swellDirectionIcon: self.getDirectionArrow(direction: (_hour.swellDirection?.values.first?.value)!),
                            windSpeed: (_hour.windSpeed?.values.first?.value)!,
                            windDirection: (_hour.windDirection?.values.first?.value)!,
                            windDirectionIcon: self.getDirectionArrow(direction: (_hour.windDirection?.values.first?.value)!)
                        ))
                    }
                    //Divide forecast by for 7 day span
                    self.showingData = Array(self.fetchedData.prefix(24))
                    for day in self.fetchedData {
                        
                        let updatedDate = Calendar.current.date(byAdding: .day, value: kont2 ,to: Date())?.formatted(date: .abbreviated, time: .omitted)
                        let exampleDate = updatedDate!.components(separatedBy: " ")[0] + " " + updatedDate!.components(separatedBy: " ")[1].prefix(3)
                        
                        if (day.time == "00" && tempData.count > 1)  {

                            self.kont2 += 1
                            self.fetchedWeeklyData.append(tempData)
                            tempData.removeAll()
                        }

                        tempData.append(weatherData(
                            time: day.time,
                            date: exampleDate,
                            airTemperature: 0,
                            waterTemperature: 0,
                            waveHeight: day.waveHeight,
                            waveDirection:  day.waveDirection,
                            waveDirectionIcon: self.getDirectionArrow(direction: (day.waveDirection)),
                            wavePeriod: day.wavePeriod,
                            swellHeight: day.swellHeight,
                            swellPeriod: day.swellPeriod,
                            swellDirection: day.swellDirection,
                            swellDirectionIcon: self.getDirectionArrow(direction: (day.swellDirection)),
                            windSpeed: day.windSpeed,
                            windDirection: day.windDirection,
                            windDirectionIcon: self.getDirectionArrow(direction: (day.windDirection))
                        ))
                        
                    }
                    self.isLoading = false
                }
                catch {
                    self.APIerror = true
                    self.isLoading = false
                }
            }
            
            tideRequest.fetch{
                
                _tideData in
                do
                {
                    let _hours = try _tideData.get().data
                    
                    for _hour in _hours
                    {
                        //Save tide levels
                        self.fetchedTideData.append(
                            tideData(time: self.simplifyTime(date: _hour.time),
                                        tide: _hour.data.values.first!))
                        if (self.fetchedTideData.count == 24) {
                            self.fetchedWeeklyTideData.append(self.fetchedTideData)
                            self.fetchedTideData.removeAll()
                        }
                    }
                    //Save the current time
                    self.fetchedCurrentTime.currentTimeString = self.initialIdx(time: editedDate.components(separatedBy: " ")[0])                    
                }
                catch {
                    self.APIerror = true
                    self.isLoading = false
                }
            }
        }
    }
    
    /// Depending on the wind direction select corresponding icon.
    /// Wind direction is inversed, if the API says 0 degrees --> Wind is goin from N to S
    func getDirectionArrow(direction: Double)-> String {
        
        if direction < 22.5 || direction > 337.5{
            return "arrow.down"
        }
        if direction > 22.5 && direction < 67.5{
            return "arrow.down.left"
        }
        if direction < 112.5 && direction > 67.5{
            return "arrow.left"
        }
        if direction > 112.5 && direction < 157.5{
            return "arrow.up.left"
        }
        if direction < 202.5 && direction > 157.5{
            return "arrow.up"
        }
        if direction > 202.5 && direction < 247.5{
            return "arrow.up.right"
        }
        if direction < 292.5 && direction > 247.5{
            return "arrow.right"
        }
        if direction > 292.5 && direction < 337.5{
            return "arrow.down.right"
        }
        
        return ""
    }
    
    func simplifyTime(date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.timeStyle = DateFormatter.Style.short
        
        let time = formatter.string(from: date)

        var shortened_time: [String] = []

        shortened_time = time.components(separatedBy: ":00")
        //Handle half hour time zones
        if shortened_time.count < 2 {
            shortened_time = time.components(separatedBy: ":30")
        }
        let total_time = shortened_time[0] + shortened_time[1]
        return total_time
    }
    
    func simplifyDate(date: Date) -> String {
        let currentDate = Calendar.current.dateComponents([.year, .month, .day], from: date)
        let date = Calendar.current.date(from: currentDate)
        
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "MMM d"
        let lastDate = formatter3.string(from: date!)
        
        if (followingDates.isEmpty || followingDates.last?.date != lastDate) {
            followingDates.append(nextDates(date: lastDate))
        }
        return lastDate
    }
    
    func initialIdx(time: String) -> String{
        var shortened_time: [String] = []
        shortened_time = time.components(separatedBy: ":")
        return shortened_time[0]
    }
    
}
