//
//  Tip.swift
//  Soorf
//
//  Created by Garat Berasategi on 5/4/24.
//

import Foundation
import TipKit

struct SearchTip: Tip {
    static let searchViewVisited = Event(id: "searchViewVisited")
    static let beachTap = Event(id: "beachTap")
    
    var title: Text{
        Text("Find your beach")
    }
    
    var message: Text?{
        Text("Tap here to search any beach in the world.")
    }
    
    var image: Image?{
        Image(systemName: "magnifyingglass")
    }
    
    var rules: [Rule]{
        #Rule(Self.searchViewVisited) {event in
            event.donations.count == 0
        }
        #Rule(Self.beachTap) {event in
            event.donations.count > 5
        }
    }
}

struct SetFavoriteTip: Tip {
    
    static let setFavorite = Event(id: "setFavorite")
    static let favoriteViewVisited = Event(id: "favoriteViewVisited")
    
    var title: Text{
        Text("View saved beaches")
    }
    
    var message: Text?{
        Text("Check out ocean forecast of your favorite beaches.")
    }
    
    var image: Image?{
        Image(systemName: "star")
    }
    
    var rules: [Rule]{
        #Rule(Self.setFavorite) {event in
            event.donations.count > 0
        }
        #Rule(Self.favoriteViewVisited) {event in
            event.donations.count > 5
        }
    }
}
