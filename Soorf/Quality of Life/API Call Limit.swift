//
//  API Call Limit.swift
//  Soorf
//
//  Created by Garat Berasategi on 22/5/23.
//

import SwiftUI

struct APIErrorHandling: View {
    var body: some View {
        ZStack{
            Color.white
            Text("Oops! Something went wrong")
                .bold()
                .foregroundColor(.cyan)
        }
        .ignoresSafeArea()
    }
}

struct API_Call_Limit_Previews: PreviewProvider {
    static var previews: some View {
        APIErrorHandling()
    }
}
