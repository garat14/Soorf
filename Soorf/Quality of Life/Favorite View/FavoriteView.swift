//
//  FavoriteView.swift
//  Soorf
//
//  Created by Garat Berasategi on 25/3/24.
//

import SwiftUI
import SwiftData
import MapKit

struct FavoriteView: View {
    @Environment(\.modelContext) var context
    @Query(sort: \FavoriteBeach.beachName) var favoriteBeaches: [FavoriteBeach] = []
    
    @EnvironmentObject private var mapControl: MapControl

    @Binding var isShowing: Bool
    
    var body: some View {
        HStack {
            ZStack{
                VStack(alignment: .leading) {
                    List{
                        Section(header:
                                Text("Favorites")
                                    .foregroundStyle(Color("Yellow Main").opacity(0.9))
                                    .font(.title)
                                    .fontWeight(.bold) +
                                Text("  ") +
                                Text(Image(systemName: "star.fill"))
                                    .foregroundStyle(Color("Yellow Main").opacity(0.9))
                                    .font(.title2)){
                            ForEach(favoriteBeaches){favoriteBeach in
                                Button(
                                    action: {
                                        mapControl.mapPosition = CLLocationCoordinate2D(latitude: CLLocationDegrees(favoriteBeach.latitude), longitude: CLLocationDegrees(favoriteBeach.longitude))
                                        isShowing.toggle()
                                        mapControl.updateMap = true},
                                    label: {
                                        HStack{
                                            Text(favoriteBeach.beachName)
                                            Spacer()
                                            Image(systemName: "location.fill")
                                        }
                                        .bold()
                                        .font(.headline)
                                        //.colorInvert()
                                        .foregroundStyle(Color("Yellow Light"))
                                    }
                                )
                                .listRowBackground(Color("Dark").opacity(0.8))
                            }
                            .onDelete{indexSet in
                                for index in indexSet{
                                    context.delete(favoriteBeaches[index])
                                }
                            }
                        }
                    }
                    .headerProminence(.increased)
                    .scrollContentBackground(.hidden)
                    .background(Color("Main").opacity(0.8))
                    .listStyle(.grouped)
                    .listRowSpacing(2)
                    .overlay{
                        if favoriteBeaches.isEmpty {
                            ContentUnavailableView(label: {
                                Label("No favorite beach is saved", systemImage: "star.slash.fill")
                                    .foregroundStyle(Color(.white).gradient.opacity(0.9))
                            })
                        }
                    }
                }
                .padding(.top, 95)
                .frame(width: 220)
            }
            Spacer()
        }
    }
    
}

#Preview {
    FavoriteView(isShowing: .constant(true))
}
