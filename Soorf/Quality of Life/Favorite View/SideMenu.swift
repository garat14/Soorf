//
//  SideMenu.swift
//  Soorf
//
//  Created by Garat Berasategi on 24/3/24.
//

import SwiftUI
import MapKit

struct SideMenu: View {
    
    @Binding var isShowing: Bool
    
    @EnvironmentObject private var mapControl: MapControl

    var edgeTransition: AnyTransition = .move(edge: .leading)
    var body: some View {
        ZStack(alignment: .bottom) {
            if (isShowing) {
                Color.clear
                    .ignoresSafeArea()
                    .onTapGesture {
                        isShowing.toggle()
                    }
                FavoriteView(isShowing: $isShowing)
                    .environmentObject(mapControl)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
        .ignoresSafeArea()
        .animation(.easeInOut, value: isShowing)
    }
}

#Preview {
    SideMenu(isShowing: .constant(true))
}
