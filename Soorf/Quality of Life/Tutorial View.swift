//
//  Tutorial View.swift
//  Soorf
//
//  Created by Garat Berasategi on 25/6/23.
//

import SwiftUI
import SwiftData

struct Tutorial_View: View {
    @Environment(\.modelContext) var context
    @Query var userData: [UserInfo]
    
    var body: some View {
        TabView{
            VStack{
                HStack{
                    Text("Tap Your Favorite Beach")
                        .font(.title)
                        .bold()
                    Spacer()
                }
                Divider()
                HStack{
                    (Text("Navigate through the map and tap the beach ") +
                     Text(Image(systemName: "beach.umbrella.fill")) +
                    Text(" icon."))
                    .font(.title3)
                    Spacer()
                }
                Spacer()
                Image("tutorialImage")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(30)
                    .shadow(radius: 10)

            }
            .padding()
            VStack{
                HStack{
                    Text("Find surfing conditions")
                        .font(.title)
                        .bold()
                    Spacer()
                }
                Divider()
                HStack{
                    (Text("Magnitude and direction of waves (") +
                     Text(Image("wavesmall")) +
                     Text(") , swell (") +
                     Text(Image(systemName: "water.waves")) +
                     Text(") and wind (") +
                     Text(Image(systemName: "wind")) +
                     Text(").")
                    )
                    .font(.title3)
                    Spacer()
                }
                Spacer()
                Image("tutorialsurfcondition")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(30)
                    .shadow(radius: 10)
            }
            .padding()
        }
        .background(Color(.systemBackground).opacity(0.7))
        .tabViewStyle(.page)
        .indexViewStyle(.page(backgroundDisplayMode: .interactive))
        .cornerRadius(20)
        .padding(25)
        .onTapGesture{
                userData[0].isTipShowing = false
        }
    }
}

#Preview {
    Tutorial_View()
}

