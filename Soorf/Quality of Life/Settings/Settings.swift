//
//  Settings.swift
//  Soorf
//
//  Created by Garat Berasategi on 27/5/24.
//

import SwiftUI
import SwiftData

struct Settings: View {
    @Environment(\.modelContext) var context
    @Query var appConfiguration: [AppConfigurations] = []
    @State var selectedTempUnit: tempUnit
    @State var selectedLengthUnit: lengthUnit
    
    @State var selectionWave: ClosedRange<CGFloat>
    @State var selectionSwell: ClosedRange<CGFloat>
    @State var selectionWind: ClosedRange<CGFloat>
    
    var body: some View {
        NavigationStack{
            List{
                Section(header: Text("Units")){
                    Picker("Temperature", selection: $selectedTempUnit) {
                        Text("ºC").tag(tempUnit.celsius)
                        Text("ºF").tag(tempUnit.farenheit)
                    }
                    Picker("Length", selection: $selectedLengthUnit) {
                        Text("m").tag(lengthUnit.meter)
                        Text("ft").tag(lengthUnit.feet)
                    }
                }
                Section(header: Text("Ideal Conditions")){
                    VStack(alignment: .leading){
                        HStack{
                                Text("Wave")
                                Image("wave")
                                    .resizable()
                                    .frame(width:20,height: 20)
                        }
                        Sliders(selection: $selectionWave)
                    }
                    VStack(alignment: .leading){
                        HStack{
                            Text("Swell")
                            Image(systemName: "water.waves")
                        }
                        Sliders(selection: $selectionSwell)
                    }
                    VStack(alignment: .leading){
                        HStack{
                            Text("Wind")
                            Image(systemName: "wind")
                        }
                        SlidersWind(selection: $selectionWind)
                    }
                }
            }
        }
        .onDisappear{
            appConfiguration[0].lengthUnit = selectedLengthUnit
            appConfiguration[0].tempUnit = selectedTempUnit
            
            appConfiguration[0].maxWave = Float(selectionWave.upperBound)
            appConfiguration[0].minWave = Float(selectionWave.lowerBound)
            appConfiguration[0].maxSwell = Float(selectionSwell.upperBound)
            appConfiguration[0].minSwell = Float(selectionSwell.lowerBound)
            appConfiguration[0].maxWind = Float(selectionWind.upperBound)
            appConfiguration[0].minWind = Float(selectionWind.lowerBound)
        }
        .presentationDetents([.height(600)])
    }
}

#Preview {
    Settings(selectedTempUnit: .celsius, selectedLengthUnit: .meter, selectionWave: 0...0,selectionSwell: 0...0, selectionWind: 0...0)
}
