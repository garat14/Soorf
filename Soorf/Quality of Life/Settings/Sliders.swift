//
//  Slider.swift
//  Soorf
//
//  Created by Garat Berasategi on 3/9/24.
//

import SwiftUI
import SwiftData

struct Sliders: View {
    @Query var appConfiguration: [AppConfigurations]
    @Binding var selection: ClosedRange<CGFloat>
    
    var body: some View {
        NavigationStack{
            VStack {
                RangeSliderView(selection: $selection, range: 0...15)
                HStack{
                    getCorrectSizeUnit(size: selection.lowerBound, unit: appConfiguration[0].lengthUnit)
                    Text(" - ")
                    getCorrectSizeUnit(size: selection.upperBound, unit: appConfiguration[0].lengthUnit)
                }
                .bold()
            }
            .navigationTitle("Settings")
        }
    }
}

struct SlidersWind: View {
    @Query var appConfiguration: [AppConfigurations]
    @Binding var selection: ClosedRange<CGFloat>
    
    var body: some View {
        NavigationStack{
            VStack {
                RangeSliderView(selection: $selection, range: 0...30)
                
                HStack{
                    getCorrectSpeedUnit(size: selection.lowerBound, unit: appConfiguration[0].lengthUnit)
                    Text(" - ")
                    getCorrectSpeedUnit(size: selection.upperBound, unit: appConfiguration[0].lengthUnit)
                }
                .bold()
            }
            //.padding()
            .navigationTitle("Settings")
        }
    }
}

struct RangeSliderView: View {
    
    @Binding var selection: ClosedRange<CGFloat>
    var range: ClosedRange<CGFloat>
    var minimumDistance: CGFloat = 0
    var tint: Color = Color("Main")
    @State private var slider1: GestureProperties = .init()
    @State private var slider2: GestureProperties = .init()
    @State private var indicatorWidth: CGFloat = 0
    @State private var isInitial: Bool = false
    
    var body: some View {
        GeometryReader{ reader in
            
            let maxSliderWidth = reader.size.width - 30
            ZStack(alignment: .leading) {
                Capsule()
                    .fill(Color("Light").opacity(0.40))
                    .frame(height: 5)
                
                /// Sliders
                HStack(spacing: 0){
                    Circle()
                        .fill(Color("Main"))
                        .frame(width: 15, height: 15)
                        .contentShape(.rect)
                        .overlay(alignment: .leading){
                            Rectangle()
                                .fill(Color("Main"))
                                .frame(width: indicatorWidth, height: 5)
                                .offset(x: 15)
                                .allowsHitTesting(false)
                        }
                        .offset(x: slider1.offset)
                        .gesture(
                            DragGesture(minimumDistance: 0)
                                .onChanged{ value in
                                    //Calculating Offset
                                    var translation = value.translation.width + slider1.lastStoredOffset
                                    translation = min(max(translation,0), slider2.offset)
                                    slider1.offset = translation
                                    
                                    calculateNewRange(reader.size)
                                }
                                .onEnded{_ in
                                    //Storing Previous Offset
                                    slider1.lastStoredOffset = slider1.offset
                                }
                        )
                    Circle()
                        .fill(Color("Main"))
                        .frame(width: 15, height: 15)
                        .contentShape(.rect)
                        .offset(x: slider2.offset)
                        .gesture(
                            DragGesture(minimumDistance: 0)
                                .onChanged{ value in
                                    //Calculating Offset
                                    var translation = value.translation.width + slider2.lastStoredOffset
                                    translation = min(max(translation, slider1.offset), maxSliderWidth)
                                    slider2.offset = translation
                                    
                                    calculateNewRange(reader.size)
                                }
                                .onEnded{_ in
                                    //Storing Previous Offset
                                    slider2.lastStoredOffset = slider2.offset
                                }
                        )
                }
            }
            .frame(maxHeight: .infinity)
            .task {
                guard !isInitial else {return}
                isInitial = true
                try? await Task.sleep(for: .seconds(0))
                let maxWidth = reader.size.width - 30
                
                /// Converting Selection Range into Offset
                let start = selection.lowerBound.interpolate(
                    inputRange: [range.lowerBound,range.upperBound],
                    outputRange: [0,maxWidth])
                
                let end = selection.upperBound.interpolate(
                    inputRange: [range.lowerBound,range.upperBound],
                    outputRange: [0,maxWidth])
                
                slider1.offset = start
                slider1.lastStoredOffset = start
                
                slider2.offset = end
                slider2.lastStoredOffset = end
                
                calculateNewRange(reader.size)
            }
        }
        .frame(height: 20)
    }
    
    private struct GestureProperties {
        var offset: CGFloat = 0
        var lastStoredOffset: CGFloat = 0
    }
    
    
    private func calculateNewRange(_ size: CGSize) {
        
        indicatorWidth = slider2.offset - slider1.offset
        
        let maxWidth = size.width - 30
        //Calculate New Range
        let startProgress = slider1.offset / maxWidth
        let endProgress = slider2.offset / maxWidth
        
        //Interpolating Between Upper and Lower Bounds
        let newRangeStart = range.lowerBound.interpolated(towards: range .upperBound, amount: startProgress)
        let newRangeEnd = range.lowerBound.interpolated(towards: range .upperBound, amount: endProgress)
        
        selection = newRangeStart...newRangeEnd

        return
    }
}



/*#Preview {
    Sliders(selection: )
}*/

/// Interpolation
extension CGFloat {
    func interpolate(inputRange: [CGFloat], outputRange: [CGFloat]) -> CGFloat {
        let x = self
        let length = inputRange.count - 1
        if x <= inputRange[0] { return outputRange[0] }
        
        for index in 1...length {
            let x1 = inputRange[index - 1]
            let x2 = inputRange[index]
            let y1 = outputRange[index - 1]
            let y2 = outputRange [index]
            
            /// Linear Interpolation Formula: y1 + ((y2-y1) / (x2-x1)) * (x-x1)
            if x <= inputRange[index] {
                let y = y1 + ((y2-y1) / (x2-x1)) * (x-x1)
                return y
            }
        }
        
        return outputRange[length]
    }
}
